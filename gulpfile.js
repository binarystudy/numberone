const gulp = require('gulp');
const sass = require('gulp-sass');
const html = require('gulp-htmlmin');

gulp.task('sass', () => {
  return gulp
    .src('src/styles/index.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public'));
});

gulp.task('html', () => {
  return gulp
    .src('src/index.html')
    .pipe(html({ collapseWhitespace: true }))
    .pipe(gulp.dest('./public'));
});

gulp.task('watch', () => {
  gulp.watch('src/styles/**/*.scss', gulp.series('sass'));
  gulp.watch('src/index.html', gulp.series('html'));
});

gulp.task('default', gulp.series('sass', 'html', 'watch'));
